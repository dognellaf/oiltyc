﻿using UnityEngine;
/*
 
    Обнаружен баг 20010031. При перемещении камеры (сенсорное управление) переодически
  
 */
public class CameraFlip : MonoBehaviour{
    private Vector3 _startPos, _dir, _currentPos;
    private float direct, directStart;
    public float CamSize;
    public bool CameraMoving = true;
    void Start() {
        _startPos = transform.position;
        direct = 0;
        directStart = 0;
    }
    private void FixedUpdate()
    {
        if (CameraMoving == true)
        {
            CamSize = Camera.main.orthographicSize;
            if (Input.GetKey(KeyCode.LeftArrow) == true)
            {
                Camera.main.transform.position = new Vector3((float)(Camera.main.transform.position.x - 0.5), Camera.main.transform.position.y, -20);
            }
            if (Input.GetKey(KeyCode.RightArrow) == true)
            {
                Camera.main.transform.position = new Vector3((float)(Camera.main.transform.position.x + 0.5), Camera.main.transform.position.y, -20);
            }
            if (Input.GetKey(KeyCode.DownArrow) == true)
            {
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, (float)(Camera.main.transform.position.y - 0.5), -20);
            }
            if (Input.GetKey(KeyCode.UpArrow) == true)
            {
                Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, (float)(Camera.main.transform.position.y + 0.5), -20);
            }
            if (Camera.main.orthographicSize <= 26)
            {
                if (Input.GetKey(KeyCode.Z) == true)
                {
                    Camera.main.orthographicSize = Camera.main.orthographicSize + (float)0.1;
                }
            }
            if (Camera.main.orthographicSize > 0.2)
            {
                if (Input.GetKey(KeyCode.X) == true)
                {
                    Camera.main.orthographicSize = Camera.main.orthographicSize - (float)0.1;
                }
            }
            if (Camera.main.orthographicSize < 1)
            {
                Camera.main.orthographicSize = (float)1.01;
            }
            if (Camera.main.orthographicSize > 40)
            {
                Camera.main.orthographicSize = (float)39.99;
            }
            if (Input.touchCount == 0)
            {
                directStart = 0;
            }
            if ((Input.touchCount == 1) && (Input.mousePosition.y > 200))
            {
                var touch = Input.GetTouch(0);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        _startPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y));
                        break;
                    case TouchPhase.Moved:
                        _currentPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y));
                        _dir = new Vector3(_startPos.x - _currentPos.x, _startPos.y - _currentPos.y);
                        transform.position = transform.position + _dir;
                        break;
                }
            }
            if (Input.touchCount > 1)
            {
                Vector3 _startPos1;
                Vector3 _startPos2;
                var touch1 = Input.GetTouch(0);
                var touch2 = Input.GetTouch(1);
                switch (touch1.phase)
                {
                    case TouchPhase.Moved:
                        _startPos1 = touch1.position;
                        _startPos2 = touch2.position;
                        if (directStart == 0)
                        {
                            directStart = Vector3.Distance(_startPos1, _startPos2);
                        }
                        else
                        {
                            direct = Vector3.Distance(_startPos1, _startPos2);
                            float Koef = directStart / direct;
                            if (Koef > 1)
                            {
                                GetComponent<Camera>().orthographicSize = GetComponent<Camera>().orthographicSize - (float)(0.5 * Koef);
                            }
                            else
                            {
                                GetComponent<Camera>().orthographicSize = GetComponent<Camera>().orthographicSize + (float)(0.5 * Koef);
                            }
                        }
                        break;
                }
            }
        }
    }
}    