﻿using UnityEngine;
using TMPro;

public class FPS : MonoBehaviour {
    short fpscounter;
    void Start() {
        fpscounter = 0;
        InvokeRepeating("FPSUpdate", 0, 1);
    }

    void Update() {
        fpscounter += 1;
    }
    void FPSUpdate() {
        GetComponent<TextMeshProUGUI>().text = string.Format("{0:0.##}", fpscounter);
        fpscounter = 0;
    }
}
