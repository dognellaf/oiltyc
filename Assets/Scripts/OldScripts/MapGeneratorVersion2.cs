﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

// вторая версия скрипта генерации (в настоящий момент не используется)

public class MapGenerator : MonoBehaviour {

    //первичное зерно мира, количество островов, центр первого острова, расстояние между островами, минимальный и макимальный шанс
    public short height, weight; //размер карты
    public byte GenerationFactor; //коэфициент генерации
    public sbyte TileSize; //размер тайлов
    public int seed; //зерно мира
    short IslandCount, FirstIslandCenterY, FirstIslandCenterX, MinChance;
    Tile land, sand, forest, rock, shallow, oil;
    void OnEnable() {
        switch (PlayerPrefs.GetInt("TextureType")) {
            case 0:
                land = Resources.Load<Tile>("Tiles/land");
                sand = Resources.Load<Tile>("Tiles/sand");
                forest = Resources.Load<Tile>("Tiles/forest");
                rock = Resources.Load<Tile>("Tiles/rocks");
                shallow = Resources.Load<Tile>("Tiles/shallow");
                oil = Resources.Load<Tile>("Tiles/oil");
                break;
            case 2:
                land = Resources.Load<Tile>("PreAlphaTiles/land");
                sand = Resources.Load<Tile>("PreAlphaTiles/sand");
                forest = Resources.Load<Tile>("PreAlphaTiles/forest");
                rock = Resources.Load<Tile>("PreAlphaTiles/rock");
                shallow = Resources.Load<Tile>("PreAlphaTiles/shallow");
                oil = Resources.Load<Tile>("PreAlphaTiles/oil");
                break;
            case 1:
                land = Resources.Load<Tile>("EarlyAlphaTiles/land");
                sand = Resources.Load<Tile>("Tiles/sand");
                forest = Resources.Load<Tile>("EarlyAlphaTiles/forest");
                rock = Resources.Load<Tile>("EarlyAlphaTiles/rock");
                shallow = Resources.Load<Tile>("PreAlphaTiles/shallow");
                oil = Resources.Load<Tile>("EarlyAlphaTiles/oil");
                break;
        }
    }
    private void FillOneCell(string[,,] _array) {
        for (short x = TileSize; x < weight - TileSize; x += TileSize) {
            for (short y = TileSize; y < height - TileSize; y += TileSize) {
                if (_array[x,y,0] == null) {
                    if ((_array[x - TileSize, y, 0] != null) && (_array[x + TileSize, y, 0] != null) && (_array[x, y - TileSize, 0] != null) && (_array[x, y + TileSize, 0] != null)) {
                        if ((_array[x - TileSize, y, 0] == "land") && (_array[x + TileSize, y, 0] == "land") && (_array[x, y - TileSize, 0] == "land") && (_array[x, y + TileSize, 0] == "land")) {
                            CreateCell(land, x, y, _array, "land", "BaseLayer");
                        }
                    }
                }
            }
        }
    } //функция заполнения единичных пробелов в карте

    private bool QuastionHaving(short x, short y, sbyte koefX, sbyte koefY, string IsAnOr, string[,,] _array, string _value)
    {
        switch (IsAnOr)
        {
            case "==":
                return (_array[x + koefX, y + koefY, 0] == _value);
            case "!=":
                return (_array[x + koefX, y + koefY, 0] != _value);
            default:
                return (_array[x + koefX, y + koefY, 0] == _value);
        }
    }

    private void GenShallowAndResourses(string[,,] _array) {
        for (short x = (short)(TileSize*2); x < height-TileSize; x += TileSize) {
            for (short y = (short)(TileSize*2); y < weight-TileSize; y += TileSize) {
                if (_array[x, y, 0] == null) {
                    if (QuastionHaving(x, y, (sbyte)-TileSize, 0, "!=", _array, null) && QuastionHaving(x, y, TileSize, 0, "!=", _array, null) && QuastionHaving(x, y, 0, (sbyte)-TileSize, "!=", _array, null) && QuastionHaving(x, y, 0, TileSize, "!=", _array, null))
                    {
                        CreateCell(land, x, y, _array, "land", "BaseLayer");
                    }
                    else
                    {
                        if (QuastionHaving(x, y, (sbyte)-TileSize, 0, "==", _array, null) || QuastionHaving(x, y, (sbyte)(-TileSize*2), 0, "==", _array, null) || QuastionHaving(x, y, TileSize, 0, "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * 2), 0, "==", _array, null))
                        {
                            CreateCell(shallow, x, y, _array, "shallow", "BaseLayer");
                        }
                        if (QuastionHaving(x, y, 0, (sbyte)-TileSize, "==", _array, null) || QuastionHaving(x, y, 0, (sbyte)(-TileSize*2), "==", _array, null) || QuastionHaving(x, y, 0, TileSize, "==", _array, null) || QuastionHaving(x, y, 0, (sbyte)(TileSize * 2), "==", _array, null))
                        {
                            CreateCell(shallow, x, y, _array, "shallow", "BaseLayer");
                        }
                        if (QuastionHaving(x, y, (sbyte)-TileSize, (sbyte)-TileSize, "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * -2), (sbyte)(TileSize * -2), "==", _array, null) || QuastionHaving(x, y, TileSize, TileSize, "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * 2), (sbyte)(TileSize * 2), "==", _array, null))
                        {
                            CreateCell(shallow, x, y, _array, "shallow", "BaseLayer");
                        }
                        if (QuastionHaving(x, y, (sbyte)-TileSize, TileSize, "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * -2), (sbyte)(TileSize * 2), "==", _array, null) || QuastionHaving(x, y, TileSize, (sbyte)(-TileSize), "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * 2), (sbyte)(-TileSize * 2), "==", _array, null))
                        {
                            CreateCell(shallow, x, y, _array, "shallow", "BaseLayer");
                        }
                        if (QuastionHaving(x, y, (sbyte)-TileSize, (sbyte)(TileSize * 2), "==", _array, null) || QuastionHaving(x, y, (sbyte)(-TileSize), (sbyte)(TileSize * -2), "==", _array, null) || QuastionHaving(x, y, TileSize, (sbyte)(TileSize * 2), "==", _array, null) || QuastionHaving(x, y, TileSize, (sbyte)(TileSize * -2), "==", _array, null))
                        {
                            CreateCell(shallow, x, y, _array, "shallow", "BaseLayer");
                        }
                        if (QuastionHaving(x, y, (sbyte)(TileSize * -2), TileSize, "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * -2), (sbyte)(-TileSize), "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * 2), TileSize, "==", _array, null) || QuastionHaving(x, y, (sbyte)(TileSize * 2), (sbyte)(-TileSize), "==", _array, null))
                        {
                            CreateCell(shallow, x, y, _array, "shallow", "BaseLayer");
                        }
                    }
                } //генерация мелководья
                if (_array[x, y, 0] == "land") {
                        int chance = Random.Range(1, 99);
                        if (chance > 75) {
                            chance = Random.Range(1, 99);
                            if (chance >= 15) {
                                if (chance > 90) {
                                    CreateCell(oil, x, y, _array, "oil", "ResoursesLayer");
                                }
                                else {
                                    CreateCell(forest, x, y, _array, "forest", "ResoursesLayer");
                                }
                            }
                            else {
                               CreateCell(rock, x, y, _array, "rock", "ResoursesLayer");
                            }
                    }
                } //генерация ресурсов
            }
        }
    } //функция генерации мелководья и ресурсов
    private void GenSand(string[,,] _array) {
        for (short x = TileSize; x < weight - TileSize; x += TileSize) {
            for (short y = TileSize; y < height - TileSize; y += TileSize) {
                if (((_array[x - TileSize, y - TileSize, 0] == "shallow") || (_array[x - TileSize, y, 0] == "shallow") || (_array[x - TileSize, y + TileSize, 0] == "shallow") || (_array[x, y - TileSize, 0] == "shallow") || (_array[x, y + TileSize, 0] == "shallow") || (_array[x + TileSize, y - TileSize, 0] == "shallow") || (_array[x + TileSize, y, 0] == "shallow") || (_array[x + TileSize, y + TileSize, 0] == "shallow")) && (_array[x, y, 0] != null)&&(_array[x, y, 0] != "shallow")) {
                    CreateCell(null, x, y, _array, null, "ResoursesLayer");
                    CreateCell(sand, x, y, _array, "sand", "BaseLayer");
                }
            }
        }
    } //функция генерации песка
    private int GenSeed() {
        string _allLocalSeed;
        int _localSeed = Random.Range(3, 9);
        IslandCount = (short)(_localSeed);
        _allLocalSeed = string.Format("{0:0.##}", _localSeed);
        _localSeed = Random.Range(10, 19);
        FirstIslandCenterX = (short)(_localSeed * 10);
        _allLocalSeed = _allLocalSeed + string.Format("{0:0.##}", _localSeed);
        _localSeed = Random.Range(10, 19);
        FirstIslandCenterY = (short)(_localSeed * 10);
        _allLocalSeed = _allLocalSeed + string.Format("{0:0.##}", _localSeed);
        _localSeed = Random.Range(30, 60);
        MinChance = (short)(_localSeed);
        _allLocalSeed = _allLocalSeed + string.Format("{0:0.##}", _localSeed);
        return int.Parse(_allLocalSeed);
    } //функция генерации сида
    private void CreateCell(Tile _type, short _x, short _y, string[,,] _array, string _typeName, string _layerName) {
            GameObject.Find(_layerName).gameObject.GetComponent<Tilemap>().SetTile(new Vector3Int(_x, _y, 0), _type);
            _array[_x, _y, 0] = _typeName;
    } //функция создания тайла
    public void NewGen() {
        seed = GenSeed(); //генерируем новое зерно
        Camera.main.transform.position = new Vector3(FirstIslandCenterX, FirstIslandCenterY, -20);
        GameObject.Find("BaseLayer").gameObject.GetComponent<Tilemap>().ClearAllTiles();
        GameObject.Find("ResoursesLayer").gameObject.GetComponent<Tilemap>().ClearAllTiles();
        GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell = new string[weight, height, 5];
        GenMap(); //генерируем карту
    } //функция новой генерации
    private void GenIsland(short _centerX, short _centerY, short _size) {
        short halfSize = (short)(_size / 2);
        short LimYmax = (short)(_centerY + halfSize < height ? _centerY + halfSize : height);
        short LimXmax = (short)(_centerX + halfSize < weight ? _centerX + halfSize : weight);
        float rastoyanie = 0;
        for (short x = (short)(_centerX - halfSize < 0 ? 0 : _centerX - halfSize); x < LimXmax; x++) {
            for (short y = (short)(_centerY - halfSize < 0 ? 0 : _centerY - halfSize); y < LimYmax; y++) {
                if ((y > _centerY) && (x > _centerX)) {
                    rastoyanie = Mathf.Sqrt(((y - _centerY) ^ 2) + ((x - _centerX) ^ 2));
                }
                if ((y > _centerY) && (x < _centerX)) {
                    rastoyanie = Mathf.Sqrt(((y - _centerY) ^ 2) + ((_centerX - x) ^ 2));
                }
                if ((y < _centerY) && (x < _centerX)) {
                    rastoyanie = Mathf.Sqrt(((_centerY - y) ^ 2) + ((_centerX - x) ^ 2));
                }
                if ((y < _centerY) && (x > _centerX)) {
                    rastoyanie = Mathf.Sqrt(((_centerY - y) ^ 2) + ((x - _centerX) ^ 2));
                }
                if (y == _centerY) {
                    rastoyanie = Mathf.Sqrt(((_centerY) ^ 2) + ((x - _centerX) ^ 2));
                }
                
                if (x == _centerX) {
                    rastoyanie = Mathf.Sqrt(((_centerY - y) ^ 2) + ((_centerX) ^ 2));
                }
                float chance = (float)((Random.Range(10, 70)) * ((_size / GenerationFactor) / rastoyanie));
                if ((chance >= MinChance)) {
                  CreateCell(land, x, y, GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell, "land", "BaseLayer");
                }
            }
        }
    }
    private void GenMap() {
        GenIsland(FirstIslandCenterX, FirstIslandCenterY, (short)(2 * Random.Range(100, 176)));
        int[] IslandsPointsX = new int[IslandCount];
        IslandsPointsX[0] = FirstIslandCenterX;
        int[] IslandsPointsY = new int[IslandCount];
        IslandsPointsY[0] = FirstIslandCenterY;
        for (byte k = 1; k < IslandCount; k++) {
            short x = (short)(Random.Range(50, 500));
            short y = (short)(Random.Range(50, 500)); 
            for (byte h = 0; h < k; h++) {
                if ((IslandsPointsX[h] == x)&&(IslandsPointsY[h] == y)) {
                    break;
                }
                else {
                    if (h == IslandCount - 1) {
                        IslandsPointsX[k] = x;
                        IslandsPointsY[k] = y;
                        GenIsland(x, y, (short)(Random.Range(100, 176)));
                    }
                }
            }
            GenIsland(x, y, (short)(Random.Range(100, 146)));
        }
        GenShallowAndResourses(GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell);
        GenSand(GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell);
        FillOneCell(GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell);
    }
    void Start() {
        if (height == 0) {
            height = 600;
        }
        if (weight == 0) {
            weight = 600;
        }
        seed = (seed == 0 ? seed = GenSeed() : seed);
        Camera.main.transform.position = new Vector3(FirstIslandCenterX, FirstIslandCenterY, -20);
        GenMap();
    }
}