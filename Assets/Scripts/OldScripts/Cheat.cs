﻿using UnityEngine;

public class Cheat : MonoBehaviour {
    private string[] cheat;
    public string DebugCheat;
    public GameObject Pashalka;
    void Start(){
        cheat = new string[6];
        for (byte i = 0; i < 6; i++) {
            cheat[i] = "0";
        }
    }
    void Update() { 
        if (Input.anyKeyDown == true) {
            DebugCheat = "";
            for (byte i = 0; i < 6; i++) {
                if (i == 5) {
                    bool flag = false;
                    string _keyCodeStr = "0";
                    if (Input.GetKey(KeyCode.A) == true) {
                        _keyCodeStr = "A";
                        flag = true;
                    }
                    if (Input.GetKey(KeyCode.B) == true) {
                        _keyCodeStr = "B";
                        flag = true;
                    }
                    if (Input.GetKey(KeyCode.C) == true) {
                        _keyCodeStr = "C";
                        flag = true;
                    }
                    if (Input.GetKey(KeyCode.D) == true) {
                        _keyCodeStr = "D";
                        flag = true;
                    }
                    if (Input.GetKey(KeyCode.E) == true) {
                        _keyCodeStr = "E";
                        flag = true;
                    }
                    if (flag == false) {
                        _keyCodeStr = "0";
                    }
                    cheat[i] = _keyCodeStr;
                    for (byte j = 0; j < 6; j++) {
                        DebugCheat = DebugCheat + cheat[j];
                    }
                }
                else {
                    cheat[i] = cheat[i + 1];
                }
            }
        }
        if (DebugCheat == "ABBABB") {
            Pashalka.SetActive(true);
        }
    }
}