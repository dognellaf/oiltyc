﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class other : MonoBehaviour, IPointerClickHandler {
    public GameObject k1,k2;
    public TextMeshProUGUI Text;
    private bool flag;
    void Start() {
        flag = false;
    }
    public void OnPointerClick(PointerEventData eventData) {
        if (flag == false) {
            flag = true;
            k2.SetActive(true);
            k1.SetActive(false);
            Text.text = "Downgrade";
        }
        else {
            flag = false;
            k1.SetActive(true);
            k2.SetActive(false);
            Text.text = "Evolve";
        }
    }
}
