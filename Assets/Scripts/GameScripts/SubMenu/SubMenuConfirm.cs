﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class SubMenuConfirm: MonoBehaviour, IPointerClickHandler
{
    public int cost;
    public Vector3Int xyz;
    public void OnPointerClick(PointerEventData eventData)
    {
        CityModuleScript CityModule = GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>();
        if (CityModule.money > cost)
        {
            GameObject.Find("ResoursesLayer").gameObject.GetComponent<Tilemap>().SetTile(xyz, null);
            GameObject.Find("BaseLayer").gameObject.GetComponent<Tilemap>().SetTile(xyz, Resources.Load<Tile>("Tiles/land"));
            CityModule.cell[xyz.x, xyz.y, 0] = "land";
            CityModule.money -= cost;
            //звук подтверждения
            Destroy(transform.parent.gameObject);
        }
        else
        {
            //звук отмены
        }
    }
}
