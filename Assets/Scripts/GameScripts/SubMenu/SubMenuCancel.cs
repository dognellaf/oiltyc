﻿using UnityEngine;
using UnityEngine.EventSystems;

public class SubMenuCancel : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        //звук подтверждения
        Destroy(transform.parent.gameObject);
    }
}
