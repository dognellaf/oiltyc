﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MapGeneratorVersion3 : MonoBehaviour
{
    public Tilemap ocean, land, resources;
    public Chank[,] chanks = new Chank[20, 20];
    void Start()
    {
        for (byte _subX = 0; _subX < 20; _subX++)
        {
            for (byte _subY = 0; _subY < 20; _subY++)
            {
                chanks[_subX, _subY] = new Chank();
                chanks[_subX, _subY].SetPosition(_subX * 16, _subY * 16);
                chanks[_subX, _subY].GenerateChank(ocean, land, resources);
            }
        }
    }
}
