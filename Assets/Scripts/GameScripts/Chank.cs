﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Chank : MonoBehaviour
{
    //координаты верхнего левого угла чанка
    int x, y;

    //тип чанка
    byte type;

    //тайлы
    static Tile land, sand, forest, rock, shallow, oil;

    //содержащиеся тайлы
    string[,] tiles = new string[16, 16];

    public void SetPosition(int _x, int _y)
    {
        x = _x;
        y = _y;
    }

    void OnEnable()
    {
        switch (PlayerPrefs.GetInt("TextureType"))
        {
            case 0:
                land = Resources.Load<Tile>("Tiles/land");
                sand = Resources.Load<Tile>("Tiles/sand");
                forest = Resources.Load<Tile>("Tiles/forest");
                rock = Resources.Load<Tile>("Tiles/rocks");
                shallow = Resources.Load<Tile>("Tiles/shallow");
                oil = Resources.Load<Tile>("Tiles/oil");
                break;
            case 2:
                land = Resources.Load<Tile>("PreAlphaTiles/land");
                sand = Resources.Load<Tile>("PreAlphaTiles/sand");
                forest = Resources.Load<Tile>("PreAlphaTiles/forest");
                rock = Resources.Load<Tile>("PreAlphaTiles/rock");
                shallow = Resources.Load<Tile>("PreAlphaTiles/shallow");
                oil = Resources.Load<Tile>("PreAlphaTiles/oil");
                break;
            case 1:
                land = Resources.Load<Tile>("EarlyAlphaTiles/land");
                sand = Resources.Load<Tile>("Tiles/sand");
                forest = Resources.Load<Tile>("EarlyAlphaTiles/forest");
                rock = Resources.Load<Tile>("EarlyAlphaTiles/rock");
                shallow = Resources.Load<Tile>("PreAlphaTiles/shallow");
                oil = Resources.Load<Tile>("EarlyAlphaTiles/oil");
                break;
        }
    }

    public bool GenerateChank(Tilemap oceanMap, Tilemap landMap, Tilemap resources)
    {
        if ((x == 0) || (y == 0))
        {
            return false;
        }
        else
        {
            int[,] intMap = CreateNoise();
            for (byte _x = 0; _x < 16; _x++)
            {
                for (byte _y = 0; _y < 16; _y++)
                {
                    switch (intMap[_x, _y])
                    {
                        case 0:
                            landMap.SetTile(new Vector3Int(x + _x, y + _y, 0), land);
                            tiles[_x, _y] = "land";
                            break;
                        case 1:
                            landMap.SetTile(new Vector3Int(x + _x, y + _y, 0), land);
                            tiles[_x, _y] = "land";
                            break;
                        case 2:
                            oceanMap.SetTile(new Vector3Int(x + _x, y + _y, 0), shallow);
                            tiles[_x, _y] = "shallow";
                            break;
                        case 3:
                            oceanMap.SetTile(new Vector3Int(x + _x, y + _y, 0), shallow);
                            tiles[_x, _y] = "shallow";
                            break;
                    }
                }
            }
            return true;
        }
    }

    private int[,] CreateNoise()
    {
        int[,] intMap = new int[16, 16];

        //цикл для генерации шума и его сглаживания
        for (byte _subX = 0; _subX < 16; _subX++)
        {
            for (byte _subY = 0; _subY < 16; _subY++)
            {
                //тип тайла
                int _typeOfTile = Random.Range(0, 4);
                //int min = -1;
                intMap[_subX, _subY] = _typeOfTile;

                //сглаживание по X
                if (_subX != 0)
                {
                    if (intMap[_subX, _subY] < intMap[_subX - 1, _subY])
                    {
                        intMap[_subX - 1, _subY] = intMap[_subX, _subY];
                    }
                }

                //сглаживание по Y
                if (_subY != 0)
                {
                    if (intMap[_subX, _subY] < intMap[_subX, _subY - 1])
                    {
                        intMap[_subX, _subY - 1] = intMap[_subX, _subY];
                    }
                }
            }
        }
        return intMap;
    }
}
