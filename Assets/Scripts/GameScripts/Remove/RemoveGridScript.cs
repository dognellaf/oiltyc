﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class RemoveGridScript : MonoBehaviour, IPointerClickHandler, IDragHandler, IPointerUpHandler
{
    public int profit;
    public byte PointsCount;
    public GameObject BaseLayer;
    public Vector3Int[] points = new Vector3Int[100];
    public TileBase tb;
    public int count;

    //при активации обнуляем данные
    void OnEnable()
    {
        profit = 0;
        PointsCount = 0;
        points = new Vector3Int[100];
    }

    //функция сдвига массива точек
    private void PointOffset(int _s, int _pc, Vector3Int[] _array)
    {
        for (int l = _s; l < _pc; l++)
        {
            if (l != _pc - 1)
            {
                _array[l] = _array[l + 1];
            }
            else
            {
                _array[l] = Vector3Int.zero;
            }
        }
    }

    //функция скрипта
    private void AllScript()
    {
        //задается позиция нажатия мыши
        Vector3Int NewPosition = GameObject.Find("CityModule").GetComponent<CityModuleScript>().GetPositionOfMouse();

        //задаются переменные поддерживаемой клетки, клетки постройки, локальной прибыли
        string SupportedCell;
        string _cell = GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell[NewPosition.x, NewPosition.y, 0];
        int _localProfit;

        //для упрощения кода задается скрипт
        ConfirmRemove ConfirmButton = GameObject.Find("ConfirmButton").GetComponent<ConfirmRemove>();

        //если клетка не является природной, то..
        if ((_cell != "sand") && (_cell != "land") && (_cell != "ocean") && (_cell != "forest") && (_cell != "shallow") && (_cell != "rock") && (_cell != "oil") && (_cell != "null"))
        {
            //проверяем постройку
            switch (_cell)
            {
                //если постройка является лесопилкой
                case "sawmill":
                    SupportedCell = "forest";
                    ConfirmButton.empty = Resources.Load<Tile>("Tiles/forest");
                    _localProfit = 20;
                    break;
                //если постройка является стандартной
                default:
                    SupportedCell = "land";
                    ConfirmButton.empty = Resources.Load<Tile>("Tiles/land");
                    _localProfit = 0;
                    break;
            }

            //задаем флаг повторения на ложь
            bool flag = false;

            //проверяем все точки на повторение
            for (byte s = 0; s < points.Length; s++)
            {

                //если точка повторяется
                if (points[s] == NewPosition)
                {
                    if (count < 35)
                    {
                        //убираем тайл на поле выбора
                        GameObject.Find("SelectLayer").gameObject.GetComponent<Tilemap>().SetTile(NewPosition, null);

                        //смещаем массив точек
                        PointOffset(s, PointsCount, points);

                        //отнимаем единицу от количества точек
                        PointsCount -= 1;

                        //отнимаем прибыль здания от общей прибыли
                        profit = profit - _localProfit;
                    }

                    //указываем, что найдено повторение
                    flag = true;
                    break;
                }
            }

            //если точек меньше 100 и не обнаружено повторение
            if ((PointsCount < 100) && (flag == false))
            {

                //устанавливаем тайл выбора
                GameObject.Find("SelectLayer").gameObject.GetComponent<Tilemap>().SetTile(NewPosition, Resources.Load<Tile>("Tiles/select"));

                //добавляем позицию в массив
                points[PointsCount] = NewPosition;

                //вносим в массив поддерживаемую клетку 
                ConfirmButton.pointsEmntyCell[PointsCount] = SupportedCell;

                //прибавляем единицу к количеству точек
                PointsCount += 1;

                //добавляем к общему доходу доход здания
                profit = profit + _localProfit;
            }
        }
    }

    //функция при нажатии
    public void OnPointerClick(PointerEventData eventData)
    {
        AllScript();
    }
    public void OnDrag(PointerEventData eventData)
    {
        count += 1;
        if (count > 35)
        {
            GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = false;
            AllScript();
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = true;
        count = 0;
    }
}
