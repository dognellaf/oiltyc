﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class ConfirmRemove : MonoBehaviour, IPointerClickHandler {
    public string[] pointsEmntyCell;
    public Tile empty;

    //при активации объекта 
    public void OnEnable() {

        //задается количество пустых ячеек
        pointsEmntyCell = new string[GameObject.Find("RemoveGrid").gameObject.GetComponent<RemoveGridScript>().points.Length];

        //задается стандартная нулевая ячейка
        empty = Resources.Load<Tile>("Tiles/land");
    }

    private void RemoveAllBuild(RemoveGridScript _bg, byte _i) {

        //загружаем поддерживаемый тайл
        Tile _SupTile = Resources.Load<Tile>("Tiles/" + pointsEmntyCell[_i]);
        if (GameObject.Find("RoadLayer").gameObject.GetComponent<Tilemap>().GetTile(_bg.points[_i]) != null)
        {
            if (GameObject.Find("RoadLayer").gameObject.GetComponent<Tilemap>().GetTile(_bg.points[_i]).name.Contains("dirtroad"))
            {
                //удаляем выбор на поле дорог
                GameObject.Find("RoadLayer").gameObject.GetComponent<Tilemap>().SetTile(_bg.points[_i], null);
            }
        }
        else {
            //устанавливаем тайл на основном поле
            GameObject.Find("BaseLayer").gameObject.GetComponent<Tilemap>().SetTile(_bg.points[_i], empty);
        }

        //удаляем выбор на поле выбора
        GameObject.Find("SelectLayer").gameObject.GetComponent<Tilemap>().SetTile(_bg.points[_i], null);

        //возвращаем стандартное значение в массив ячеек
        GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell[_bg.points[_i].x, _bg.points[_i].y, 0] = _SupTile.name;
    }

    //функция при нажатии
    public void OnPointerClick(PointerEventData eventData) {

        //включаем движение камеры
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = true;

        //задаем скрипт для упрощения кода
        RemoveGridScript RG = GameObject.Find("RemoveGrid").gameObject.GetComponent<RemoveGridScript>();

        //цикл для удаления построек
        for (byte i = 0; i < RG.PointsCount; i++) {
            RemoveAllBuild(RG, i);
        }

        //если массив не нулевой
        if (RG.points[0] != Vector3.zero) {

            //снимаем деньги на снос
            GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().money -= 100;
        }

        //отнимаем доход из общего дохода игры
        GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().profit -= RG.profit;

        //отключаем сетку сноса
        GameObject.Find("RemoveGrid").gameObject.SetActive(false);
    }
}
