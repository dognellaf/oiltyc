﻿using UnityEngine;
using UnityEngine.EventSystems;
public class RemoveModuleScript : MonoBehaviour, IPointerClickHandler {
    public CityModuleScript CityModule;

    public void OnPointerClick(PointerEventData eventData) {
        if (CityModule.MenuActiveCheck("RemoveGrid") == false)
        {
            CityModule.UndisableMenu("RemoveGrid");
        }
        else
        {
            CityModule.UndisableMenu("");
        }
    }
}
