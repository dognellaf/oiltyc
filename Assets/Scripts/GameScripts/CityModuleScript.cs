﻿using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;

public class CityModuleScript : MonoBehaviour {
    /* [x,y,n]
      n - различные характеристики:
      0 - название тайла
      1 - является ли тайл частью канализации
      2 - является ли тайл частью водопровода
      3 - есть ли у здания электричество
      4 - к какой зоне относится тайл
    */
    public string[,,] cell = new string[1002, 1002, 5]; //массив ячеек
    public Vector3Int[] roadsArray; //массив дорог
    public int profit, money; //большие переменные для количества денег и прибыли
    public byte HomeDemand = 100, ShopsDemand = 100, HeavyDemand = 100; //спросы на дома, магазины, заводы
    public GameObject ZoneMenu, BuildGrid, RemoveGrid, BuildMenu, ZoneGrid, LayersMenu;
    public CancelBuild Cancel;
    byte count;

    //функция получения позиции мыши
    public Vector3Int GetPositionOfMouse() {
        return (new Vector3Int((int)Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z), Camera.main.stereoActiveEye).x, (int)Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z), Camera.main.stereoActiveEye).y, 0));
    }

    //функция проверки активности объектов
    public bool MenuActiveCheck(string _ActiveName)
    {
        switch (_ActiveName)
        {
            case "BuildGrid":
                return (BuildGrid.activeSelf);
            case "ZoneMenu":
                return (ZoneMenu.activeSelf);
            case "RemoveGrid":
                return (RemoveGrid.activeSelf);
            case "BuildMenu":
                return (BuildMenu.activeSelf);
            case "ZoneGrid":
                return (ZoneGrid.activeSelf);
            case "LayersMenu":
                return (LayersMenu.activeSelf);
            default:
                return (false);
        }
    }

    //функция отключения подменю
    public void UndisableMenu(string _ActiveName)
    {
        switch (_ActiveName)
        {
            case "BuildGrid":
                ZoneMenu.SetActive(false);
                BuildMenu.SetActive(false);
                RemoveGrid.SetActive(false);
                BuildGrid.SetActive(true);
                ZoneGrid.SetActive(false);
                LayersMenu.SetActive(false);
                break;
            case "ZoneMenu":
                ZoneMenu.SetActive(true);
                BuildMenu.SetActive(false);
                RemoveGrid.SetActive(false);
                BuildGrid.SetActive(false);
                ZoneGrid.SetActive(false);
                LayersMenu.SetActive(false);
                Cancel.ClearPreBuild();
                break;
            case "RemoveGrid":
                ZoneMenu.SetActive(false);
                BuildMenu.SetActive(false);
                RemoveGrid.SetActive(true);
                BuildGrid.SetActive(false);
                ZoneGrid.SetActive(false);
                LayersMenu.SetActive(false);
                Cancel.ClearPreBuild();
                break;
            case "BuildMenu":
                ZoneMenu.SetActive(false);
                BuildMenu.SetActive(true);
                RemoveGrid.SetActive(false);
                BuildGrid.SetActive(false);
                ZoneGrid.SetActive(false);
                LayersMenu.SetActive(false);
                Cancel.ClearPreBuild();
                break;
            case "ZoneGrid":
                ZoneMenu.SetActive(false);
                BuildMenu.SetActive(false);
                RemoveGrid.SetActive(false);
                BuildGrid.SetActive(false);
                ZoneGrid.SetActive(true);
                LayersMenu.SetActive(false);
                Cancel.ClearPreBuild();
                break;
            case "LayersMenu":
                ZoneMenu.SetActive(false);
                BuildMenu.SetActive(false);
                RemoveGrid.SetActive(false);
                BuildGrid.SetActive(false);
                ZoneGrid.SetActive(false);
                LayersMenu.SetActive(true);
                Cancel.ClearPreBuild();
                break;
            default:
                ZoneMenu.SetActive(false);
                BuildMenu.SetActive(false);
                RemoveGrid.SetActive(false);
                BuildGrid.SetActive(false);
                ZoneGrid.SetActive(false);
                LayersMenu.SetActive(false);
                Cancel.ClearPreBuild();
                break;
        }
    }

    private void GiveMoney() {
        money = money + profit;
    }
    public void UpdateVal(float _Val, string _GameObjectName, string _AddValStr) {
        if (_Val < (1000)) {
            GameObject.Find(_GameObjectName).GetComponent<TextMeshProUGUI>().text = _AddValStr + string.Format("{0:0.0##}", _Val) + " $";
        }
        if (_Val >= (1000) && _Val < (1000000)) {
            GameObject.Find(_GameObjectName).GetComponent<TextMeshProUGUI>().text = _AddValStr + string.Format("{0:0.0##}", _Val / (1000)) + "k" + " $";
        }
        if (_Val >= (1000000) && _Val < (1000000000)) {
            GameObject.Find(_GameObjectName).GetComponent<TextMeshProUGUI>().text = _AddValStr + string.Format("{0:0.0##}", _Val / (1000000)) + "M" + " $";
        }
    }
    void Start() {
        //        if (PlayerPrefs.GetInt("Money") == 0)/
        //       {
        //            money = 1000;
        //        }
        //        else
        //        {
        //            money = PlayerPrefs.GetInt("Money");
        //        }
        InvokeRepeating("GiveMoney", 0, 1);
        MapGenerator MP = GameObject.Find("GameMaster").gameObject.GetComponent<MapGenerator>();
        cell = new string[MP.weight + MP.TileSize, MP.height + MP.TileSize, 5];
    }
    void Update() {
        UpdateVal(money, "MoneyText", "");
        UpdateVal(profit, "MoneyProfitText", "+");
    }
    void FixedUpdate()
    {
        count += 1;
        if (count > 200)
        {
            CreateBuildings();
            count = 0;
        }
        GameObject.Find("HomeDemandText").GetComponent<TextMeshProUGUI>().text = "Дома: " + string.Format("{0}", HomeDemand);
        GameObject.Find("ShopsDemandText").GetComponent<TextMeshProUGUI>().text = "Магазины: " + string.Format("{0}", ShopsDemand);
        GameObject.Find("HeavyDemandText").GetComponent<TextMeshProUGUI>().text = "Заводы: " + string.Format("{0}", HeavyDemand);
    }
    private void CreateBuildings()
    {
        byte chanceHomes = (byte)(Random.Range(1, 45) * HomeDemand/40);
        Tile HomeView = Resources.Load<Tile>("Tiles/zone");
        if (chanceHomes > (byte)(Random.Range(45, 70)))
        {
            for (short i = 0; i < 302; i++)
            {
                bool flag = false;
                for (short j = 0; j < 302; j++)
                {
                    if ((cell[i,j,4] == "LowHome")&&(cell[i, j, 0] != "Home1") && (cell[i, j, 0] != "Home2") && (cell[i, j, 0] != "Home3"))
                    {
                        byte Home = (byte)Random.Range(1, 3);
                        switch (Home)
                        {
                            case 1:
                                if (cell[i + 1, j, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home1/home1");
                                }
                                else if (cell[i, j + 1, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home1/home1_90");
                                }
                                else if (cell[i - 1, j, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home1/home1_180");
                                }
                                else
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home1/home1_270");
                                }
                                break;
                            case 2:
                                if (cell[i+1,j,0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home2/home2");
                                }
                                else if (cell[i, j + 1, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home2/home2_90");
                                }
                                else if (cell[i - 1, j, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home2/home2_180");
                                }
                                else
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home2/home2_270");
                                }
                                break;
                            case 3:
                                if (cell[i + 1, j, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home3/home3");
                                }
                                else if (cell[i, j + 1, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home3/home3_90");
                                }
                                else if (cell[i - 1, j, 0].Contains("road") == true)
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home3/home3_180");
                                }
                                else
                                {
                                    HomeView = Resources.Load<Tile>("Tiles/Buildings/Homes/home3/home3_270");
                                }
                                break;
                        }
                        GameObject.Find("BuildingLayer").GetComponent<Tilemap>().SetTile(new Vector3Int(i,j,0),HomeView);
                        cell[i, j, 0] = "Home" + string.Format("{0}", Home);
                        flag = true;
                        break;
                    }
                }
                if (flag == true)
                {
                    HomeDemand -= (byte)Random.Range(1, 10);
                    break;
                }
            }
        }
    }
}