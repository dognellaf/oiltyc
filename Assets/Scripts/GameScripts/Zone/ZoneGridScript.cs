﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class ZoneGridScript : MonoBehaviour, IPointerClickHandler {
    public string SelectedZone;

    //функция проверки дороги рядом
    private bool HaveRoadOrNo(CityModuleScript _city, Vector3Int _pos, sbyte _koefX, sbyte _koefY) {
        return (_city.cell[_pos.x + _koefX, _pos.y + _koefY, 0].Contains("dirtroad") && (_city.cell[_pos.x, _pos.y, 0] == "land"));
    }

    private void RoadCheck(CityModuleScript _city, Vector3Int _Npos, Tilemap _zlayer, Tile _zone, sbyte _koefX, sbyte _koefY) {
        if (HaveRoadOrNo(_city, _Npos, _koefX, _koefY) == true) {

            //задаем флаг отсутствия дороги и переменную для проверки дорог
            bool flag = false;
            sbyte k = 0;

            //пока слева есть дорога, создаем зону
            while (flag == false) {
                Vector3Int Pos = Vector3Int.zero;
                if (_koefX != 0) {
                    Pos = new Vector3Int(_Npos.x, _Npos.y + k, _Npos.z);
                }
                else {
                    Pos = new Vector3Int(_Npos.x + k, _Npos.y, _Npos.z);
                }
                if (HaveRoadOrNo(_city, Pos, _koefX, _koefY) == true) {

                    //ставим тайл зоны
                    _zlayer.GetComponent<Tilemap>().SetTile(Pos, _zone);

                    //заносим зону в массив
                    _city.cell[Pos.x, Pos.y, 4] = SelectedZone;

                    //прибавляем единицу к переменной проверки дорог
                    k += 1;
                }
                else {
                    flag = true;
                }
            }

            flag = false;
            k = -1;
            while (flag == false) {
                Vector3Int Pos = Vector3Int.zero;
                if (_koefX != 0) {
                    Pos = new Vector3Int(_Npos.x, _Npos.y + k, _Npos.z);
                }
                else {
                    Pos = new Vector3Int(_Npos.x + k, _Npos.y, _Npos.z);
                }
                if (HaveRoadOrNo(_city, Pos, _koefX, _koefY) == true) {

                    //ставим тайл зоны
                    _zlayer.GetComponent<Tilemap>().SetTile(Pos, _zone);

                    //заносим зону в массив
                    _city.cell[Pos.x, Pos.y, 4] = SelectedZone;

                    //прибавляем единицу к переменной проверки дорог
                    k -= 1;
                }
                else {
                    flag = true;
                }
            }
        }
    }
    
    private void AllScript()
    {
        //для упрощения кода задаем скрипт, карту тайлов и стандартный тайл для зоны
        CityModuleScript CityModule = GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>();
        Tilemap ZoneLayer = GameObject.Find("ZoneLayer").gameObject.GetComponent<Tilemap>();
        Tile ZoneTile = Resources.Load<Tile>("Tiles/zone");

        //меняем цвет тайла в зависимости от значения SelectedZone
        switch (SelectedZone)
        {
            case "LowHome":
                ZoneTile.color = new Color(0, 1, (float)0.039, (float)0.55);
                break;
            case "LowShop":
                ZoneTile.color = new Color(0, (float)0.6256, 1, (float)0.55);
                break;
            case "LowFactory":
                ZoneTile.color = new Color(1, (float)0.9696, 0, (float)0.55);
                break;
        }

        //получаем позицию мыши
        Vector3Int NewPosition = CityModule.GetPositionOfMouse();

        if (CityModule.cell[NewPosition.x, NewPosition.y, 4] == null)
        {
            //провереяем каждую из сторон для дороги
            RoadCheck(CityModule, NewPosition, ZoneLayer, ZoneTile, -1, 0);
            RoadCheck(CityModule, NewPosition, ZoneLayer, ZoneTile, 1, 0);
            RoadCheck(CityModule, NewPosition, ZoneLayer, ZoneTile, 0, 1);
            RoadCheck(CityModule, NewPosition, ZoneLayer, ZoneTile, 0, -1);
        }
        else
        {
            ZoneLayer.GetComponent<Tilemap>().SetTile(NewPosition, null);
            CityModule.cell[NewPosition.x, NewPosition.y, 4] = null;
        }
    }

    public void OnPointerClick(PointerEventData eventData) {
        AllScript();
    }
}
