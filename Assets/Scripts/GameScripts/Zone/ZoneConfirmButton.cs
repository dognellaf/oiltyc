﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ZoneConfirmButton : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData) {

        //включаем движение камеры
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = true;

        GameObject.Find("ZoneGrid").SetActive(false);
    }
}
