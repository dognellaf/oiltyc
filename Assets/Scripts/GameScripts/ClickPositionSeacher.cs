﻿using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;

public class ClickPositionSeacher : MonoBehaviour {
    public Vector3Int MousePoistion;
    public GameObject OMPrefab;
    public Tilemap Layer;
    public bool LookCell; 
    public string CellName;
    public TileBase TileName;
    void FixedUpdate() {
        MousePoistion = GameObject.Find("CityModule").GetComponent<CityModuleScript>().GetPositionOfMouse();
        if (LookCell == false) {
            TileName = Layer.GetTile(new Vector3Int(MousePoistion.x, MousePoistion.y, 0));
        }
        else {
            if ((MousePoistion.x < 1000) && (MousePoistion.y < 1000))
            {
                CellName = GameObject.Find("CityModule").GetComponent<CityModuleScript>().cell[MousePoistion.x, MousePoistion.y, 0];
            }
            else
            {
                CellName = "Позиция за пределами массива.";
            }
        }
        if ((Input.GetKey(KeyCode.Mouse0) == true)&&(GameObject.Find("OM[" + string.Format("{0:000}", MousePoistion.x) + "," + string.Format("{0:000}", MousePoistion.y) + "]") == null))
        {
            switch (CellName)
            {
                case "forest":
                    GameObject gmf = GameObject.Instantiate(OMPrefab);
                    gmf.transform.SetParent(GameObject.Find("MapUI").gameObject.transform);
                    gmf.name = "OM[" + string.Format("{0:000}", MousePoistion.x) + "," + string.Format("{0:000}", MousePoistion.y)+"]";
                    gmf.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().cost = 50;
                    gmf.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().xyz = new Vector3Int(MousePoistion.x, MousePoistion.y,0);
                    gmf.transform.position = new Vector3(MousePoistion.x + 0.5f, MousePoistion.y - 0.5f, 0);
                    gmf.transform.localScale = new Vector3(0.01f, 0.01f, 0);
                    gmf.gameObject.transform.Find("OMObjName").gameObject.GetComponent<TextMeshProUGUI>().text = "Лес";
                    gmf.gameObject.transform.Find("OMConfirmBut").transform.Find("OMConfirmButText").gameObject.GetComponent<TextMeshProUGUI>().text = "Вырубить лес (50$)?";
                    //Debug.Log("Forest return");
                    break;
                case "rock":
                    GameObject gmr = GameObject.Instantiate(OMPrefab);
                    gmr.transform.SetParent(GameObject.Find("MapUI").gameObject.transform);
                    gmr.name = "OM[" + string.Format("{0:000}", MousePoistion.x) + "," + string.Format("{0:000}", MousePoistion.y)+"]";
                    gmr.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().cost = 3000;
                    gmr.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().xyz = new Vector3Int(MousePoistion.x, MousePoistion.y, 0);
                    gmr.transform.position = new Vector3(MousePoistion.x + 0.5f, MousePoistion.y - 0.5f, 0);
                    gmr.transform.localScale = new Vector3(0.01f, 0.01f, 0);
                    gmr.gameObject.transform.Find("OMObjName").gameObject.GetComponent<TextMeshProUGUI>().text = "Скалы";
                    gmr.gameObject.transform.Find("OMConfirmBut").transform.Find("OMConfirmButText").gameObject.GetComponent<TextMeshProUGUI>().text = "Взорвать скалы (3000$)?";
                    //Debug.Log("Rock return");
                    break;
                case "shallow":
                    GameObject gms = GameObject.Instantiate(OMPrefab);
                    gms.transform.SetParent(GameObject.Find("MapUI").gameObject.transform);
                    gms.name = "OM[" + string.Format("{0:000}", MousePoistion.x) + "," + string.Format("{0:000}", MousePoistion.y) + "]";
                    gms.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().cost = 5000;
                    gms.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().xyz = new Vector3Int(MousePoistion.x, MousePoistion.y, 0);
                    gms.transform.position = new Vector3(MousePoistion.x + 0.5f, MousePoistion.y - 0.5f, 0);
                    gms.transform.localScale = new Vector3(0.01f, 0.01f, 0);
                    gms.gameObject.transform.Find("OMObjName").gameObject.GetComponent<TextMeshProUGUI>().text = "Мелководье";
                    gms.gameObject.transform.Find("OMConfirmBut").transform.Find("OMConfirmButText").gameObject.GetComponent<TextMeshProUGUI>().text = "Осушить (5000$)?";
                    break;
                case "oil":
                    GameObject gmo = GameObject.Instantiate(OMPrefab);
                    gmo.transform.SetParent(GameObject.Find("MapUI").gameObject.transform);
                    gmo.name = "OM[" + string.Format("{0:000}", MousePoistion.x) + "," + string.Format("{0:000}", MousePoistion.y) + "]";
                    gmo.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().cost = 3500;
                    gmo.transform.Find("OMConfirmBut").gameObject.GetComponent<SubMenuConfirm>().xyz = new Vector3Int(MousePoistion.x, MousePoistion.y, 0);
                    gmo.transform.position = new Vector3(MousePoistion.x + 0.5f, MousePoistion.y - 0.5f, 0);
                    gmo.transform.localScale = new Vector3(0.01f, 0.01f, 0);
                    gmo.gameObject.transform.Find("OMObjName").gameObject.GetComponent<TextMeshProUGUI>().text = "Нефть";
                    gmo.gameObject.transform.Find("OMConfirmBut").transform.Find("OMConfirmButText").gameObject.GetComponent<TextMeshProUGUI>().text = "Засыпать (3500$)?";
                    break;
            }
        }
    }
}