﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class CancelBuild : MonoBehaviour, IPointerClickHandler {
    public GameObject BuildGrid;
    
    //функция нажатия на кнопку
    public void OnPointerClick(PointerEventData eventData) {
        ClearPreBuild();
    }
    public void ClearPreBuild() {
        //для упрощения
        BuildGridScript BG = BuildGrid.GetComponent<BuildGridScript>();

        //включаем движение камеры
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = true;

        //в цикле удаляем спрайты с поля предпостройки
        for (byte i = 0; i < BG.PointsCount; i++) {

            //задаем тайлмап для упрощения кода
            Tilemap TM = GameObject.Find("PreBuildLayer").gameObject.GetComponent<Tilemap>();

            //удаляем тайл на поле предпостройки
            TM.SetTile(BG.points[i], null);
        }

        //очищаем все переменные
        BG.NullAll();

        //отключаем сетку постройки
        BuildGrid.SetActive(false);
    }
}
