﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class RoadBuild : MonoBehaviour {
    public void RoadUpdate(short k, string _roadName) {
        string[,,] cell = GetComponent<CityModuleScript>().cell;
        Vector3Int[] roadXY = GetComponent<CityModuleScript>().roadsArray;
        for (short i = 0; i < k; i++) {
            bool left = (cell[roadXY[i].x - 1, roadXY[i].y, 0] == _roadName) || (cell[roadXY[i].x - 1, roadXY[i].y, 0] == _roadName + "UP_RIGHT") || (cell[roadXY[i].x - 1, roadXY[i].y, 0] == _roadName + "DOWN_RIGHT");
            bool right = (cell[roadXY[i].x + 1, roadXY[i].y, 0] == _roadName) || (cell[roadXY[i].x + 1, roadXY[i].y, 0] == _roadName + "DOWN_LEFT") || (cell[roadXY[i].x + 1, roadXY[i].y, 0] == _roadName + "UP_LEFT");
            bool down = (cell[roadXY[i].x, roadXY[i].y - 1, 0] == _roadName + "1") || (cell[roadXY[i].x, roadXY[i].y - 1, 0] == _roadName + "UP_RIGHT") || (cell[roadXY[i].x, roadXY[i].y - 1, 0] == _roadName + "UP_LEFT");
            bool up = (cell[roadXY[i].x, roadXY[i].y + 1, 0] == _roadName + "1") || (cell[roadXY[i].x, roadXY[i].y + 1, 0] == _roadName + "DOWN_RIGHT") || (cell[roadXY[i].x, roadXY[i].y + 1, 0] == _roadName + "DOWN_LEFT");
            if (left && right && down && up) {
                CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "CROSSROAD", cell);
            }
            else {
                bool flag = false;
                if (up && left && down) {
                    CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "UP_LEFT_DOWN", cell);
                    flag = true;
                }
                if (up && right && down) {
                    CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "UP_RIGHT_DOWN", cell);
                    flag = true;
                }
                if (left && right && up) {
                    CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "UP_LEFT_RIGHT", cell);
                    flag = true;
                }
                if (left && right && down) {
                    CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "RIGHT_DOWN_LEFT", cell);
                    flag = true;
                }
                if (flag == false) {
                    if (left && up) {
                        CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "UP_LEFT", cell);
                        flag = true;
                    }
                    if (left && down) {
                        CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "DOWN_LEFT", cell);
                        flag = true;
                    }
                    if (up && right) {
                        CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "UP_RIGHT", cell);
                        flag = true;
                    }
                    if (down && right) {
                        CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName, "DOWN_RIGHT", cell);
                        flag = true;
                    }
                    if (flag == false) {
                        if (left && right) {
                            CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName,"", cell);
                        }
                        if (up && down) {
                            CreateTileOnSelectLayer((short)roadXY[i].x, (short)roadXY[i].y, _roadName,"1", cell);
                        }
                    }
                }
            }
        }
    }
    private void CreateTileOnSelectLayer(short _x, short _y, string _roadName, string _subname, string[,,] _cell){
        GameObject.Find("PreBuildLayer").gameObject.GetComponent<Tilemap>().SetTile(new Vector3Int(_x, _y, 0), LoadRoadTile(_subname, _roadName));
        _cell[_x, _y, 0] = _roadName + _subname;
    }
    private Tile LoadRoadTile(string _name, string _roadName){
        Tile _tile = Resources.Load<Tile>("Tiles/Roads/" + _roadName + "/" + _roadName + _name);
        return _tile;
    }
}
