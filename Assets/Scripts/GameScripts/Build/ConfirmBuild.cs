﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;

public class ConfirmBuild : MonoBehaviour, IPointerClickHandler {

    //функция добавления тайла
    private void CreateTile(TileBase _selTile, string _layer, short _i, BuildGridScript _bg) {
        //очищается тайл на поле предпостройки
        GameObject.Find("PreBuildLayer").gameObject.GetComponent<Tilemap>().SetTile(_bg.points[_i], null);

        //задаем в массив ячеек название тайла
        GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell[_bg.points[_i].x, _bg.points[_i].y, 0] = _selTile.name;

        //устанавливаем тайл на основном поле
        GameObject.Find(_layer).gameObject.GetComponent<Tilemap>().SetTile(_bg.points[_i], _selTile);
    }

    //функция завершения строительства
    private void ConfirmAllBuild(BuildGridScript _bg, short _i) {

        //берется тайл с поля предпостройки
        TileBase SelectTile = GameObject.Find("PreBuildLayer").gameObject.GetComponent<Tilemap>().GetTile(_bg.points[_i]);

        //если строим дорогу, то постройку ставим на слое для дорог, иначе на базовом слое
        if (SelectTile.name.Contains("road")) {
            CreateTile(Resources.Load<Tile>("Tiles/asphalt"), "BaseLayer", _i, _bg);
            CreateTile(SelectTile, "RoadLayer", _i, _bg);
        }
        else {
            CreateTile(SelectTile, "BuildingLayer", _i, _bg);
        }
    }

    //при нажатии на кнопку
    public void OnPointerClick(PointerEventData eventData) {

        //включаем движение камеры
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = true;

        //задаем необходимый для постройки скрипт
        BuildGridScript BG = GameObject.Find("BuildGrid").gameObject.GetComponent<BuildGridScript>();

        // если нет долга
        if (BG.Debt == false) {

            //завершаем постройку для каждой точки
            for (byte i = 0; i < BG.PointsCount; i++) {
                ConfirmAllBuild(BG, i);

                //добавляем прибыль и вычитаем стоимность
                GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().money -= BG.cost;
                GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().profit += BG.profit;
            }

            //отключаем сетку строительства
            GameObject.Find("BuildGrid").gameObject.SetActive(false);
        }
    }
}
