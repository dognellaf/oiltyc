﻿using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;
using TMPro;

public class BuildGridScript : MonoBehaviour, IPointerClickHandler, IDragHandler, IPointerUpHandler {
    public string SupportedCell, BuildCell;
    public BuildingFlip BF;
    public bool Debt;
    public byte PointsCount;
    public int profit, cost;
    public int count = 0;
    public Vector3Int[] points = new Vector3Int[100];
    public string TileName;
    int sum;
    byte k;
    Vector3Int NewPosition;
    void OnEnable()
    {
        NullAll();
        transform.Find("BuildGridSumText").gameObject.SetActive(true);
        transform.Find("BuildGridSumText").gameObject.GetComponent<TextMeshProUGUI>().text = "Сумма постройки: 0 $";
        transform.Find("BuildGridSumText").gameObject.GetComponent<TextMeshProUGUI>().color = Color.green;
        transform.Find("BuildGridText").gameObject.GetComponent<TextMeshProUGUI>().text = "Режим постройки";
        transform.Find("BuildGridText").gameObject.GetComponent<TextMeshProUGUI>().color = Color.green;
    }
    public void NullAll()
    {
        BF.FlipCount = 0;
        PointsCount = 0;
        k = 0;
        sum = 0;
        cost = 0;
        profit = 0;
        points = new Vector3Int[100];
    }
    private void PointOffset(byte _s, byte _pc, Vector3Int[] _array)
    {
        for (byte l = (byte)(_s); l < _pc; l++)
        {
            if (l != _pc - 1)
            {
                _array[l] = _array[l + 1];
            }
            else
            {
                _array[l] = Vector3Int.zero;
            }
        }
    }
    public void SetSupportedCell(string _type, int _cost, int _profit)
    {
        SupportedCell = _type;
        cost = _cost;
        profit = _profit;
    }

    //функция основного функционала скрипта
    private void AllScript()
    {
        //задаем необходимые для кода локальные переменные
        Tilemap PreBuildLayer = GameObject.Find("PreBuildLayer").gameObject.GetComponent<Tilemap>();
        GameObject CityModule = GameObject.Find("CityModule").gameObject;
        short FlipCount = GameObject.Find("FlipButton").gameObject.GetComponent<BuildingFlip>().FlipCount;
        Tile _buildTile = GameObject.Find("BuildModule").gameObject.GetComponent<TileLoader>().LoadTile(FlipCount);

        //получаем позицию мышки
        NewPosition = CityModule.GetComponent<CityModuleScript>().GetPositionOfMouse();

        //если мышка находится на поддерживаемой клетке, то выполняем код
        if ((GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell[NewPosition.x, NewPosition.y, 0] == SupportedCell)||(GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().cell[NewPosition.x, NewPosition.y, 0] == _buildTile.name))
        {

            //устанавливаем флаг повторения выбранной клетки ложным
            bool flag = false;

            //проверяем каждую выбранную точку на повторение
            for (byte s = 0; s < points.Length; s++)
            {
                //если точка повторяется
                if (points[s] == NewPosition) {
                    if (count < 35) {
                        //удаляем тайл в позиции мышки
                        PreBuildLayer.SetTile(NewPosition, null);

                        //смещаем все точки в массиве
                        PointOffset((byte)s, PointsCount, points);

                        //если мы строим дорогу
                        if (TileName == "dirtroad")
                        {

                            //смещаем точки в массиве дорог
                            PointOffset((byte)s, k, GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().roadsArray);

                            //отнимаем единицу от количества дорог
                            k -= 1;
                        }

                        //отнимаем от суммарной стоимности стоимность одной постройки
                        sum = sum - cost;

                        //отнимаем единицу от количества точек
                        PointsCount -= 1;
                    }

                    //флаг повторения истинный
                    flag = true;

                    //прерываем цикл
                    break;
                }
            }

            //если выбрано менее 100 точек в сумме и точка не повторяетсяс
            if ((PointsCount < 100) && (flag == false))
            {

                //если мы строим дорогу
                if (TileName == "dirtroad")
                {

                    //добавляем позицию в массив дорог
                    CityModule.GetComponent<CityModuleScript>().roadsArray[k] = new Vector3Int(NewPosition.x, NewPosition.y, 0);

                    //добавляем единицу к коэффициенту дорог
                    k += 1;
                }
                _buildTile = GameObject.Find("BuildModule").gameObject.GetComponent<TileLoader>().LoadTile(FlipCount);
                if (_buildTile == null)
                {
                    _buildTile = Resources.Load<Tile>("Tiles/nullcell");

                    //ставим тайл постройки на поле предпросмотра
                    PreBuildLayer.SetTile(NewPosition, _buildTile);
                }
                else
                {
                    //ставим тайл постройки на поле предпросмотра
                    PreBuildLayer.SetTile(NewPosition, _buildTile);

                    //заносим название дороги в массив клеток
                    CityModule.GetComponent<CityModuleScript>().cell[NewPosition.x, NewPosition.y, 0] = PreBuildLayer.GetTile(NewPosition).name;
                }

                //добавляем точку в массив точек
                points[PointsCount] = NewPosition;

                if (TileName == "dirtroad")
                {
                    //обновляем все дороги
                    CityModule.GetComponent<RoadBuild>().RoadUpdate(k, TileName);
                }

                //прибавляем к итоговой стоимности стоимность постройки
                sum = sum + cost;

                //добавляем единицу к коэффициенту точек
                PointsCount += 1;
            }

            //обновляем сумму построек
            CityModule.GetComponent<CityModuleScript>().UpdateVal(sum, "BuildGridSumText", "");
        }

        //если сумма привышает общее количество денежных средств, то включаем "долг" и все надписи становятся красными, иначе отключаем "долг" и делаем надписи зелеными
        if (sum > CityModule.GetComponent<CityModuleScript>().money)
        {
            transform.Find("BuildGridText").gameObject.GetComponent<TextMeshProUGUI>().color = Color.red;
            transform.Find("BuildGridSumText").gameObject.GetComponent<TextMeshProUGUI>().color = Color.red;
            Debt = true;
        }
        else
        {
            transform.Find("BuildGridText").gameObject.GetComponent<TextMeshProUGUI>().color = Color.green;
            transform.Find("BuildGridSumText").gameObject.GetComponent<TextMeshProUGUI>().color = Color.green;
            Debt = false;
        }
    }

    //функция нажатия
    public void OnPointerClick(PointerEventData eventData) {
        AllScript();
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = true;
        count = 0;
    }

    //функция перетаскивания
    public void OnDrag(PointerEventData eventData)
    {
        count += 1;
        if (count > 35)
        {
            GameObject.Find("Main Camera").gameObject.GetComponent<CameraFlip>().CameraMoving = false;
            AllScript();
        }
    }
}