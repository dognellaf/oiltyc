﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class BuildButtonScript : MonoBehaviour, IPointerClickHandler {
    /*
[n,o]:
n - Номер постройки
o - Свойства:
  0 - Название
  1 - Цена
  2 - Поддерживаемая клетка
*/
    public CityModuleScript CityModule;
    public string[,] BuildingOptions = new string[7, 4];
    public GameObject BuildMenu, BuildGrid, OkeyButton, BuildingPrefab;
    public GameObject[] BuildingsMenuList = new GameObject[7];
    public bool SubMenuBe;
    public byte NumberOfSubMenu;

    void Start() {
        BuildingsMenuList = new GameObject[7];
    }

    //создание подменю для объекта
    void CreatePrefabForBuilding(short _NumberOfBuildingInList, short _NumberOfBuildingInSubMenu, string _BuildingName, string _TileName, string _MainCell, string _SubText, string _Cost, string _Profit, string _SubName, bool _Enable, bool _HaveSub, bool _HaveSubSub, Sprite _texture) {
        
        //создаем дубликат объекта
        if (_NumberOfBuildingInSubMenu == 0) {
            BuildingsMenuList[_NumberOfBuildingInList] = Instantiate(BuildingPrefab, new Vector3(Screen.width / 2, (315 * Screen.height/384) - 2 * _NumberOfBuildingInSubMenu * BuildingPrefab.GetComponent<RectTransform>().rect.height, 0), new Quaternion(0, 0, 0, 0), BuildMenu.transform);
        }
        else {
            BuildingsMenuList[_NumberOfBuildingInList] = Instantiate(BuildingPrefab, new Vector3(Screen.width / 2, (315 * Screen.height / 384) - 2 * _NumberOfBuildingInSubMenu * BuildingPrefab.GetComponent<RectTransform>().rect.height, 0), new Quaternion(0, 0, 0, 0), BuildMenu.transform);
        }

        //меняем имя объекта
        BuildingsMenuList[_NumberOfBuildingInList].name = "MenuBuilding(" + _BuildingName + ")";
        
        //заносим имя объекта в массив
        BuildingOptions[_NumberOfBuildingInList, 0] = _BuildingName;

        //если подменю недоступно, отключаем возможность постройки здания
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().enabled = _Enable;

                                              //Работа с подобъектами подменю

        //устанавливаем имя объекта в надписи подменю
        BuildingsMenuList[_NumberOfBuildingInList].transform.Find("BMPBuildingName").gameObject.GetComponent<TextMeshProUGUI>().text = _BuildingName;

        //устанавливаем наличие папки и подпапки
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().HaveSub = _HaveSub;
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().HaveSubSub = _HaveSubSub;
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().SubName = _SubName;

        //устанавливаем цену объекта
        BuildingsMenuList[_NumberOfBuildingInList].transform.Find("BMPBuildingCost").gameObject.GetComponent<TextMeshProUGUI>().text = _Cost + " $";

        //вносим цену объекта в массив
        BuildingOptions[_NumberOfBuildingInList, 1] = _Cost;

        //передаем цену объекта в следующий скрипт
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().cost = int.Parse(_Cost);


        //устанавливаем доход объекта
        BuildingsMenuList[_NumberOfBuildingInList].transform.Find("BMPBuildingSubText").gameObject.GetComponent<TextMeshProUGUI>().text = _SubText;

        //вносим доход объекта в массив
        BuildingOptions[_NumberOfBuildingInList, 2] = _Profit;

        //передаем доход объекта в следующий скрипт
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().profit = int.Parse(_Profit);



        //устанавливаем текстуру предпросмотра постройки
        BuildingsMenuList[_NumberOfBuildingInList].transform.Find("BMPBuildingTexture").gameObject.GetComponent<Image>().sprite = _texture;

        //передаем поддерживаемый тип клеток в следующий скрипт
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().SupportedCell = _MainCell;

        //вносим поддерживаемый тип клеток в массив
        BuildingOptions[_NumberOfBuildingInList, 3] = _MainCell;

        //задаем стандартный объект сетки постройки
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().Grid = BuildGrid;

        //задаем стандартную кнопку подтверждения
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().OkeyButton = OkeyButton;

        //передаем в следующий скрипт название клетки постройки
        BuildingsMenuList[_NumberOfBuildingInList].GetComponent<BuildGridEnable>().tilename = _TileName;
    }

    //удаление подменю при переключении на другую вкладку
    private void DestroyBuildingsMenu() {
        for (short i = 0; i < BuildingsMenuList.Length; i++)
        {
            Destroy(BuildingsMenuList[i]);
        }
    }


    void Update() {
        if (SubMenuBe == true) {
            switch (NumberOfSubMenu) {
                case 1:
                    DestroyBuildingsMenu();
                    CreatePrefabForBuilding(4,0,"Грунтовая дорога", "dirtroad", "land", "Самая базовая дорога", "200", "0", "Roads", true, true, true, Resources.Load<Sprite>("Tiles/Roads/dirtroad/textures/dirtroad"));
                    CreatePrefabForBuilding(5,1, "Дорога с низкой плотностью", "smallroad1", "land", "Недоступно" ,"500", "0", "Roads", false, true, true, Resources.Load<Sprite>("Buildings/smallroad1"));
                    SubMenuBe = false;
                    break;
                case 2:
                    DestroyBuildingsMenu();
                    CreatePrefabForBuilding(0,0, "Лесопилка", "sawmill", "forest", string.Format("Позволяет получать {0}$ каждую секунду", "20"), "200", "20", "Buildings", true, true, true, Resources.Load<Sprite>("Tiles/Buildings/sawmill/textures/sawmill"));
                    SubMenuBe = false;
                    break;
                case 3:
                    DestroyBuildingsMenu();
                    CreatePrefabForBuilding(6, 0, "Маленький дом", "grass", "null", "Недоступно", "10", "0", "", false, false, false, null);
                    SubMenuBe = false;
                    break;
            }
        }
    }

    //обработка нажатий на кнопку 
    public void OnPointerClick(PointerEventData eventData) {
        //Если меню стройки не активно, то включаем первую вкладку (дороги), включаем меню стройки и активируем скрипт создания подменю
        if (CityModule.MenuActiveCheck("BuildMenu") == false)
        {
            NumberOfSubMenu = 1;
            BuildMenu.SetActive(true);
            SubMenuBe = true;
            CityModule.UndisableMenu("BuildMenu");
        }
        else
        {
            DestroyBuildingsMenu();
            CityModule.UndisableMenu("");
        }

        //если меню стройки активно, то деактивируем постройки и удаляем все подменю
    }
}
