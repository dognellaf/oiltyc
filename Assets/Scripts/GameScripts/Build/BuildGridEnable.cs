﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BuildGridEnable: MonoBehaviour, IPointerClickHandler { 
    public GameObject Grid, OkeyButton;
    public string SupportedCell, tilename, SubName;
    public int cost, profit;
    public bool HaveSub, HaveSubSub;
    public void OnPointerClick(PointerEventData eventData) {

        if (GameObject.Find("CityModule").gameObject.GetComponent<CityModuleScript>().money > cost) {

            //удаляем все подменю
            for (short i = 0; i < GameObject.Find("BuildModule").GetComponent<BuildButtonScript>().BuildingsMenuList.Length; i++)
            {
                Destroy(GameObject.Find("BuildModule").GetComponent<BuildButtonScript>().BuildingsMenuList[i]);
            }

            //отключаем меню постройки
            GameObject.Find("BuildMenu").SetActive(false); 

            //включаем сетку постройки
            Grid.SetActive(true);

            //передаем скрипту данные о типе клетки, цене, доходе, названии
            Grid.GetComponent<BuildGridScript>().SetSupportedCell(SupportedCell, cost, profit);
            Grid.GetComponent<BuildGridScript>().TileName = tilename;

            //передаем данные загрузчику тайлов
            GameObject.Find("BuildModule").gameObject.GetComponent<TileLoader>().SetOptions(tilename, SubName, HaveSub, HaveSubSub);
        }
    }
}
