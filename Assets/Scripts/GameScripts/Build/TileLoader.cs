﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class TileLoader : MonoBehaviour {
    string TileName, SubTileName;
    bool HaveSub, HaveSubSub;
    public void SetOptions(string _TileName, string _SubTileName, bool _HaveSub, bool _HaveSubSub)
    {
        TileName = _TileName;
        SubTileName = _SubTileName;
        HaveSub = _HaveSub;
        HaveSubSub = _HaveSubSub;
    }
    public Tile LoadTile(short _FlipCount) {
        if (GameObject.Find("BuildGrid").gameObject.GetComponent<BuildGridScript>().TileName == "dirtroad") {
            switch (_FlipCount) {
                case 0:
                    _FlipCount = 0;
                    break;
                case 90:
                    _FlipCount = 1;
                    break;
                case 180:
                    _FlipCount = 0;
                    break;
                case 270:
                    _FlipCount = 1;
                    break;
            }
        }
        Tile answer;
        if (HaveSub == true) {
            if (HaveSubSub == true) {
                switch (_FlipCount) {
                    case 0:
                        answer = Resources.Load<Tile>("Tiles/" + SubTileName + "/" + TileName + "/" + TileName);
                        break;
                    default:
                        answer = Resources.Load<Tile>("Tiles/" + SubTileName + "/" + TileName + "/" + TileName + string.Format("{0}",_FlipCount));
                        break;
                }
            }
            else {
                switch (_FlipCount) {
                    case 0:
                        answer = Resources.Load<Tile>("Tiles/" + SubTileName + "/" + TileName);
                        break;
                    default:
                        answer = Resources.Load<Tile>("Tiles/" + SubTileName + "/" + TileName + string.Format("{0}", _FlipCount));
                        break;
                }
            }
        }
        else {
            switch (_FlipCount) {
                case 0:
                    answer = Resources.Load<Tile>("Tiles/" + TileName);
                    break;
                default:
                    answer = Resources.Load<Tile>("Tiles/" + TileName + string.Format("{0}", _FlipCount));
                    break;
            }
        }
        return answer;
    }
}
