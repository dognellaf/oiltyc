﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public class BuildingFlip: MonoBehaviour, IPointerClickHandler {
    public BuildGridScript Grid;
    public short FlipCount = 0;
    private void TileCheck()
    {
        GameObject FlipImage = GameObject.Find("FlipImage");
        Tile _tl = GameObject.Find("BuildModule").gameObject.GetComponent<TileLoader>().LoadTile(FlipCount);
        if (_tl != null)
        {
            FlipImage.GetComponent<Image>().sprite = _tl.sprite;
        }
        else
        {
            FlipImage.GetComponent<Image>().sprite = Resources.Load<Tile>("Tiles/nullcell").sprite;
        }
    }
    void FixedUpdate() {
        TileCheck();
    }
    public void OnPointerClick(PointerEventData eventData) {
        if (FlipCount == 270) {
            FlipCount = 0;
        }
        else {
            FlipCount += 90;
        }
        TileCheck();
    }
}
