﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class GenButton : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler {
    public void OnPointerEnter(PointerEventData eventData) {
        transform.Find("NewGenButtonText").GetComponent<TextMeshProUGUI>().color = Color.red;
    }
    public void OnPointerExit(PointerEventData eventData) {
        transform.Find("NewGenButtonText").GetComponent<TextMeshProUGUI>().color = Color.white;
    }
    public void OnPointerClick(PointerEventData eventData) {
        GameObject.Find("CityModule").GetComponent<CityModuleScript>().money = 1000;
        GameObject.Find("CityModule").GetComponent<CityModuleScript>().profit = 0;
        GameObject.Find("GameMaster").GetComponent<MapGenerator>().NewGen();
    }
}