﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HomeZoneButtonScript : MonoBehaviour, IPointerClickHandler
{
    public ZoneGridScript ZoneGrid;
    public CityModuleScript CityModule;
    public void OnPointerClick(PointerEventData eventData)
    {
        CityModule.UndisableMenu("ZoneGrid");
        ZoneGrid.SelectedZone = "LowHome";
    }
}
