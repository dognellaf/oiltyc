﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ZoneButtonScript : MonoBehaviour, IPointerClickHandler {
    public CityModuleScript CityModule;

    public void OnPointerClick(PointerEventData eventData) {
        if (CityModule.MenuActiveCheck("ZoneMenu") == false)
        {
            CityModule.UndisableMenu("ZoneMenu");
        }
        else
        {
            CityModule.UndisableMenu("");
        }
    }
}