﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HeavyZoneButtonScript : MonoBehaviour, IPointerClickHandler {
    public ZoneGridScript ZoneGrid;
    public CityModuleScript CityModule;
    public void OnPointerClick(PointerEventData eventData) {
        CityModule.UndisableMenu("ZoneGrid");
        ZoneGrid.SelectedZone = "LowFactory";
    }
}