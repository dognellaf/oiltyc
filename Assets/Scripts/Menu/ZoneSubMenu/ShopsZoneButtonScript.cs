﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ShopsZoneButtonScript : MonoBehaviour, IPointerClickHandler {
    public ZoneGridScript ZoneGrid;
    public CityModuleScript CityModule;
    public void OnPointerClick(PointerEventData eventData) {
        CityModule.UndisableMenu("ZoneGrid");
        ZoneGrid.SelectedZone = "LowShop";
    }
}