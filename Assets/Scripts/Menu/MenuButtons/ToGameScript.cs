﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ToGameScript : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler {
    public GameObject loadingUI;
    public TextMeshProUGUI loadingText;
    bool flag = false;
    byte count = 0;
    byte count2 = 0;
    public void OnPointerEnter(PointerEventData eventData) {
        GetComponent<TextMeshProUGUI>().color = Color.red;
    }
    public void OnPointerExit(PointerEventData eventData) {
        GetComponent<TextMeshProUGUI>().color = Color.white;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        loadingUI.SetActive(true);
        flag = true;
        //InvokeRepeating("ChangeText", (float)0.2, (float)0.2);
    }
    void LateUpdate()
    {
        InvokeRepeating("ChangeText", (float)1.9, (float)1.9);
    }
    private void ChangeText()
    {
            switch (count2)
            {
                case 0:
                    loadingText.text = "Загрузка .";
                    break;
                case 1:
                    loadingText.text = "Загрузка . . ";
                    break;
                case 2:
                    loadingText.text = "Загрузка . . . ";
                    break;
            }
        if (count2 == 3)
        {
            count2 = 0;
        }
        else
        {
            count2++;
        }
    }
    void FixedUpdate()
    {
        if (flag == true)
        {
            count++;
        }
        if (count > 40)
        {
            SceneManager.LoadScene(1);
        }
    }
}
