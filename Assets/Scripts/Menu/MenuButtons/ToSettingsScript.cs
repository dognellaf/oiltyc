﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class ToSettingsScript : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler {
    public GameObject SettingsMenu;
    public void OnPointerEnter(PointerEventData eventData) {
        GetComponent<TextMeshProUGUI>().color = Color.red;
    }
    public void OnPointerExit(PointerEventData eventData) {
        GetComponent<TextMeshProUGUI>().color = Color.white;
    }
    public void OnPointerClick(PointerEventData eventData) {
        SettingsMenu.SetActive(true);
    }
}
