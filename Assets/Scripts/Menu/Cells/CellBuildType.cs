public enum CellBuildType
{
    All,
    Land,
    Forest,
    Rock,
    Shallow,
    Ocean,
    Nothing
}
