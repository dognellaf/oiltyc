using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Cell", menuName = "������� ����� ������", order = 0)]
public class CellObject : ScriptableObject
{
    [SerializeField] private Sprite image;
    [SerializeField] private CellBuildType type;
    [SerializeField] private new string name;
    public Sprite Image
    {
        get
        {
            return image;
        }
    }
    public string Name
    {
        get
        {
            return name;
        }
    }
    public CellBuildType BuildType
    {
        get
        {
            return type;
        }
    }
}
