﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuInteractions : MonoBehaviour
{
    [SerializeField] GameObject settingsMenu;
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject loader;

    public void QuitGame() => Application.Quit();

    public void BackToMenu() => ActiveAndColor(new Color(0.6f, 0.8509805f, 0.9176471f), mainMenu, settingsMenu);

    public void EnterToLoader()
    {
        ActiveAndColor(Color.black, loader, mainMenu);
        StartCoroutine("loaderFunc");
    }

    private void ActiveAndColor(Color color, GameObject element, GameObject previous = null)
    {
        if (previous != null)
            previous.SetActive(false);

        element.SetActive(true);
        Camera.main.backgroundColor = color;
    }

    private IEnumerator loaderFunc()
    {
        int step = 0;
        var loading = SceneManager.LoadSceneAsync(1);
        var textComponent = loader.transform.Find("Loading Text").GetComponent<TextMeshProUGUI>();
        while (!loading.isDone)
        {
            yield return new WaitForSeconds(0.2f);
            step++;
            switch (step)
            {
                case 0:
                    textComponent.text = $"Загрузка.";
                    break;
                case 1:
                    textComponent.text = $"Загрузка..";
                    break;
                case 2:
                    textComponent.text = $"Загрузка...";
                    step = -1;
                    break;
            }
        }
    }
}
