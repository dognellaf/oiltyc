﻿using UnityEngine;
using UnityEngine.EventSystems;

public class HousesButtonScript : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData) {
        GameObject.Find("BuildModule").gameObject.GetComponent<BuildButtonScript>().NumberOfSubMenu = 3;
        GameObject.Find("BuildModule").gameObject.GetComponent<BuildButtonScript>().SubMenuBe = true;
    }
}