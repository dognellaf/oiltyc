﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingsButtonScript : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData){
        GameObject.Find("BuildModule").gameObject.GetComponent<BuildButtonScript>().NumberOfSubMenu = 2;
        GameObject.Find("BuildModule").gameObject.GetComponent<BuildButtonScript>().SubMenuBe = true;
    }
}