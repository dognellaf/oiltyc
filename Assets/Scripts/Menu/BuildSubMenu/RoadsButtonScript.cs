﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RoadsButtonScript : MonoBehaviour, IPointerClickHandler {
    public void OnPointerClick(PointerEventData eventData) {
        GameObject.Find("BuildModule").gameObject.GetComponent<BuildButtonScript>().NumberOfSubMenu = 1;
        GameObject.Find("BuildModule").gameObject.GetComponent<BuildButtonScript>().SubMenuBe = true;
    }
}