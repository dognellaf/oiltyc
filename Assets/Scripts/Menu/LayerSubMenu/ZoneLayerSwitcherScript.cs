﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ZoneLayerSwitcherScript : MonoBehaviour, IPointerClickHandler {
    public GameObject ZoneLayer;
    public TextMeshProUGUI TMP;
    public void OnPointerClick(PointerEventData eventData) {
        ZoneLayer.SetActive(!ZoneLayer.activeSelf);
        if (ZoneLayer.activeSelf == true)
        {
            TMP.text = "Спрятать слой для зон";
            TMP.color = Color.red;
        }
        else
        {
            TMP.text = "Отобразить слой для зон";
            TMP.color = Color.green;
        }
    }
}
