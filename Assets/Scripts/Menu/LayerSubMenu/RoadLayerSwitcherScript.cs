﻿using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class RoadLayerSwitcherScript : MonoBehaviour, IPointerClickHandler {
    public GameObject RoadLayer;
    public TextMeshProUGUI TMP;
    public void OnPointerClick(PointerEventData eventData) {
        RoadLayer.SetActive(!RoadLayer.activeSelf);
        if (RoadLayer.activeSelf == true)
        {
            TMP.text = "Спрятать слой для дорог";
            TMP.color = Color.red;
        }
        else
        {
            TMP.text = "Отобразить слой для дорог";
            TMP.color = Color.green;
        }
    }
}
