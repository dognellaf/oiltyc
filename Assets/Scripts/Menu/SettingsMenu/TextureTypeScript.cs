﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TextureTypeScript : MonoBehaviour, IPointerUpHandler {
    void OnEnable() {
        GetComponent<Slider>().value = PlayerPrefs.GetInt("TextureType");
    }
    void Update() {
        if (GetComponent<Slider>().value == 0) {
            GameObject.Find("VersionText").gameObject.GetComponent<TextMeshProUGUI>().text = "Alpha";
        }
        else if (GetComponent<Slider>().value == 1) {
            GameObject.Find("VersionText").gameObject.GetComponent<TextMeshProUGUI>().text = "EarlyAlpha";
        }
        else
        {
            GameObject.Find("VersionText").gameObject.GetComponent<TextMeshProUGUI>().text = "PreAlpha";
        }
    }
    public void OnPointerUp(PointerEventData eventData) {
        PlayerPrefs.SetInt("TextureType", (int)GetComponent<Slider>().value);
        GetComponent<Slider>().value = PlayerPrefs.GetInt("TextureType");
        PlayerPrefs.Save();
    }
}
