﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ResolutionScript : MonoBehaviour
{
    public GameObject Dropdown;
    public int width;
    public int height;
    void OnEnable()
    {
        width = Screen.currentResolution.width;
        height = Screen.currentResolution.height;
        switch (Screen.currentResolution.width)
        { 
            case 400:
                if (Screen.currentResolution.height == 800)
                {
                    Dropdown.GetComponent<TMP_Dropdown>().value = 0;
                }
                break;
            case 720:
                if (Screen.currentResolution.height == 1280)
                {
                    Dropdown.GetComponent<TMP_Dropdown>().value = 1;
                }
                break;
            case 1080:
                if (Screen.currentResolution.height == 1920)
                {
                    Dropdown.GetComponent<TMP_Dropdown>().value = 2;
                }
                if (Screen.currentResolution.height == 2160)
                {
                    Dropdown.GetComponent<TMP_Dropdown>().value = 3;
                }
                break;
            case 1440:
                if (Screen.currentResolution.height == 2960)
                {
                    Dropdown.GetComponent<TMP_Dropdown>().value = 4;
                }
                break;
            default:
                Dropdown.GetComponent<TMP_Dropdown>().value = 0;
                break;
        }
    }
    void FixedUpdate()
    {
        int value = Dropdown.GetComponent<TMP_Dropdown>().value;
        switch (value)
        {
            case 0:
                Screen.SetResolution(400, 800, true);
                break;
            case 1:
                Screen.SetResolution(720, 1280, true);
                break;
            case 2:
                Screen.SetResolution(1080, 1920, true);
                break;
            case 3:
                Screen.SetResolution(1080, 2160, true);
                break;
            case 4:
                Screen.SetResolution(1440, 2960, true);
                break;
            default:
                break;
        }
    }
}
