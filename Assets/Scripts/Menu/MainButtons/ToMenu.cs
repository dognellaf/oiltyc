﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class ToMenu : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler {
    public void OnPointerEnter(PointerEventData eventData) {
        transform.Find("BackButtonText").GetComponent<TextMeshProUGUI>().color = Color.red;
    }
    public void OnPointerExit(PointerEventData eventData) {
        transform.Find("BackButtonText").GetComponent<TextMeshProUGUI>().color = Color.white;
    }
    public void OnPointerClick(PointerEventData eventData) {
        SceneManager.LoadScene(0);
    }
}