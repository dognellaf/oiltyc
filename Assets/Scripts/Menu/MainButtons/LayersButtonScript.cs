﻿using UnityEngine;
using UnityEngine.EventSystems;

public class LayersButtonScript : MonoBehaviour, IPointerClickHandler {
    public CityModuleScript CityModule;
    public void OnPointerClick(PointerEventData eventData) {
        if (CityModule.MenuActiveCheck("LayersMenu") == false)
        {
            CityModule.UndisableMenu("LayersMenu");
        }
        else
        {
            CityModule.UndisableMenu("");
        }
    }
}
