﻿using UnityEngine;
using UnityEngine.UI;

public class MenuAnimation : MonoBehaviour {
    public short TrX, maxX;
    public GameObject cloud, settingsMenu, loadingUI;
    float speed;
    void OnEnabled()
    {
        loadingUI.SetActive(false);
    }
    void Start() {
        speed = Screen.width / Random.Range(140, 280);
        if (!name.Contains("cloud")) {
            for (int i = 1; i < Random.Range(100, 130); i++) {
                GameObject obj = Instantiate(cloud);
                obj.name = "cloud" + string.Format("{0}", i);
                obj.GetComponent<MenuAnimation>().TrX = (short)Random.Range(-700, - 250);
                obj.GetComponent<MenuAnimation>().maxX = (short)Random.Range(Screen.width + 250, Screen.width + 700);
                obj.transform.SetParent(GameObject.Find("SecondUI").gameObject.transform);
                obj.transform.position = new Vector3(Random.Range(-300, Screen.width + 150), Random.Range(0, Screen.height), Random.Range(0, 500));
                obj.transform.localScale = new Vector3(Random.Range(4, 6), Random.Range(3, 6), (float)0.0001);
                float alpha = Random.Range(60, 100);
                float clr = Random.Range(25, 100);
                obj.GetComponent<Image>().color = new Color(clr / 100, clr / 100, clr / 100, alpha / 100);
                speed = Screen.width / Random.Range(140, 280);
            }
        }
    }

    void Update() {
        if ((name.Contains("cloud"))&&(settingsMenu.activeSelf == false)) {
            transform.position = new Vector3(transform.position.x + speed, transform.position.y, transform.position.z);
            if (transform.position.x > maxX) {
                transform.position = new Vector3(TrX, Random.Range(0, Screen.height), transform.position.z);
                float clr = Random.Range(25, 100);
                float alpha = Random.Range(60, 100);
                GetComponent<Image>().color = new Color(clr / 100, clr / 100, clr / 100, alpha/100);
                speed = Screen.width / Random.Range(70, 140);
            }
        }
    }
}
